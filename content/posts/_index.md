---
keywords:
- blog
- news
- opinion
- Voice chatbots
- terraform
- infrastructure
- nodejs
- nodeconfEU
title: Fathom News and Views
description: "We offer our opinions and insights on news in the travel and
technolgy industries. We offer technical advice and insights on AWS cloud,
all things nodejs and reactjs. We give our thoughts on infrastructure
provisioning, devops, CI/CD."
---
